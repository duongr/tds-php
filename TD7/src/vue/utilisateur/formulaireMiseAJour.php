<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Mettre à jour l'utilisateur</title>
</head>
<body>
<h1>Mettre à jour l'utilisateur : <?= htmlspecialchars($utilisateur->getLogin()) ?></h1>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour">
    <input type="hidden" name="controleur" value="utilisateur">

    <fieldset>
        <legend>Formulaire de mise à jour :</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
            <input class="InputAddOn-field" type="text" name="login" id="login_id" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label>
            <input class="InputAddOn-field" type="text" name="prenom" id="prenom_id" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label>
            <input class="InputAddOn-field" type="text" name="nom" id="nom_id" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Ancien Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Nouveau Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp3_id">Confimation du nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp3" id="mdp3_id" required>
        </p>

        <p>
            <input type="submit" value="Mettre à jour">
        </p>
    </fieldset>
</form>
</body>
</html>
