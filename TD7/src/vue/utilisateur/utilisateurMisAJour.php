<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Utilisateur mis à jour</title>
</head>
<body>
<p>L'utilisateur de login <?= htmlspecialchars($login) ?> a bien été mis à jour.</p>
<?php require __DIR__ . '/liste.php';?>
</body>
</html>
