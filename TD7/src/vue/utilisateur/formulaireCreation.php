<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire d'inscription</title>
</head>
<body>

<form method="get">
    <input type="hidden" name="action" value="creerDepuisFormulaire">

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="login_id">Login&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="nom" id="nom_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="prenom" id="prenom_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="email_id">Email&#42;</label>
        <input class="InputAddOn-field" type="email" placeholder="Ex : exemple@mail.com" name="email" id="email_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="telephone_id">Téléphone&#42;</label>
        <input class="InputAddOn-field" type="tel" placeholder="Ex : 0123456789" name="telephone" id="telephone_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="adresse_id">Adresse&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : 123 Rue Exemple" name="adresse" id="adresse_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
        <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
        <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
    </p>



    <p class="InputAddOn">
        <input class="InputAddOn-item" type="submit" value="Envoyer">
    </p>
</form>

</body>
</html>
