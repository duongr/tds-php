<!DOCTYPE html>
<html lang="fr">
<body>
<u><h1>Liste des Trajets:</h1></u>
<style>
    li {
        display: flex; /* Utilisation de Flexbox pour aligner les éléments */
        align-items: center;
        margin-bottom: 10px; /* Ajoute un peu d'espace entre les éléments de la liste */
    }
    .mettreAJour {
        margin-left: 10px;
    }
    .supprimer {
        margin-left: 10px; /* Espace entre le nom de l'utilisateur et le lien "Supprimer" */
        color: red; /* Change la couleur du lien "Supprimer" en rouge */
    }
    .detail {
        margin-left: 10px;
    }
</style>
<ul>
    <?php foreach ($trajets as $trajet): ?>
        <li>
            De : <?php echo htmlspecialchars($trajet->getDepart()); ?> à <?php echo htmlspecialchars($trajet->getArrivee()); ?><br>
            Date : <?php echo htmlspecialchars($trajet->getDate()->format('Y-m-d')); ?><br>
            Prix : <?php echo htmlspecialchars($trajet->getPrix()); ?>€
            <a class="detail" href="controleurFrontal.php?action=afficherDetail&controleur=trajet&id=<?php echo $trajet->getId(); ?>">Détails</a>
            <a class="mettreAJour" href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=trajet&id=<?= htmlspecialchars($trajet->getId()) ?>">Mettre à jour</a>
            <a class="supprimer" href="controleurFrontal.php?action=supprimer&controleur=trajet&id=<?= urlencode($trajet->getId()) ?>"
               onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce trajet ?');">Supprimer</a>
        </li>
    <?php endforeach; ?>
</ul>
<p><a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet">Créer un Trajet</a></p>
</body>
</html>
