<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../ressources/css/navstyle.css">
    <title>Préférences</title>
</head>
<body>
<header>

</header>
<main>
    <form method="get" action="controleurFrontal.php">
        <input type="hidden" name="action" value="enregistrerPreference">
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
            <?php use App\Covoiturage\Lib\PreferenceControleur;

            echo (PreferenceControleur::lire() == "utilisateur") ? 'checked' : ''; ?>>
        <label for="utilisateurId">Utilisateur</label>

        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
            <?php echo (PreferenceControleur::lire() == "trajet") ? 'checked' : ''; ?>>
        <label for="trajetId">Trajet</label>
        <input class="InputAddOn-item" type="submit" value="Envoyer">
    </form>

</main>
<footer>

</footer>

</body>
