<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../ressources/css/navstyle.css">
    <title><?php
        /**
         * @var string $titre
         */echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <a href="controleurFrontal.php?action=afficherFormulairePreference&controleur=utilisateur"><img src="../ressources/img/heart.png" alt="Image de coeur" id="heart"></a>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <a href="controleurFrontal.php"><img src="../ressources/img/maison.png" alt="Image de maison" class="maison" width="40px"> </a>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <a href="controleurFrontal.php?action=afficherFormulaireCreation"><img src="../ressources/img/add-user.png" alt="Image de bonhomme"></a>
        </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Raf
    </p>
</footer>
</body>
</html>

