<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVueGenerale(string $cheminCorpsVue, array $parametres = []) : void {
        extract($parametres);
        require __DIR__ . '/../vue/vueGenerale.php';
    }

    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);
        require __DIR__ . '/../vue/' . $cheminVue;
    }

    public static function afficherFormulairePreference()
    {
        self::afficherVueGenerale("formulairePreference.php");
    }

    public static function enregistrerPreference(): void {
        if (isset($_GET['controleur_defaut'])) {
            $preference = $_GET['controleur_defaut'];
            PreferenceControleur::enregistrer($preference);
            self::afficherVue('preferenceEnregistree.php', ['message' => 'La préférence de contrôleur est enregistrée !']);
        } else {
            self::afficherErreur('Erreur : La préférence de contrôleur n\'a pas été renseignée.');
        }
    }
}