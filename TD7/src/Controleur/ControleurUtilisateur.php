<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
class ControleurUtilisateur extends ControleurGenerique {


    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();        self::afficherVueGenerale('utilisateur/liste.php', [
            'utilisateurs' => $utilisateurs,
            'titre' => 'Liste des utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php'
        ]);
    }

    public static function afficherDetail() : void {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if ($utilisateur) {
                self::afficherVueGenerale('utilisateur/detail.php', [
                    'utilisateurEnParametre' => $utilisateur,
                    'titre' => 'Détail de l\'utilisateur',
                    'cheminCorpsVue' => 'utilisateur/detail.php'
                ]);
            } else {
                self::afficherVueGenerale('utilisateur/erreur.php', [
                    'messageErreur' => "Utilisateur avec le login '" . ($login) . "' introuvable.",
                    'titre' => 'Erreur',
                    'cheminCorpsVue' => 'utilisateur/erreur.php'
                ]);
            }
        } else {
            self::afficherVueGenerale('utilisateur/erreur.php', [
                'messageErreur' => "Aucun login spécifié.",
                'titre' => 'Erreur',
                'cheminCorpsVue' => 'utilisateur/erreur.php'
            ]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVueGenerale('utilisateur/formulaireCreation.php', [
            'titre' => 'Créer un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ]);
    }


    public static function creerDepuisFormulaire() : void {
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom']) && isset($_GET['mdp'])) {
            if ($_GET['mdp'] == $_GET['mdp2']) {
                $login = $_GET['login'];
                $nom = $_GET['nom'];
                $prenom = $_GET['prenom'];
                $mdp = $_GET['mdp'];

                // Création d'une instance de l'utilisateur
                $nouvelUtilisateur = new Utilisateur($login, $nom, $prenom, $mdp);

                // Appel de la méthode générique pour ajouter l'utilisateur à la base de données
                (new UtilisateurRepository())->ajouter($nouvelUtilisateur);

                // Récupérer la liste mise à jour des utilisateurs
                $utilisateurs = (new UtilisateurRepository())->recuperer();

                // Afficher la vue utilisateurCree.php qui contient le message et la liste
                self::afficherVueGenerale('utilisateur/utilisateurCree.php', [
                    'utilisateurs' => $utilisateurs,
                    'titre' => 'Utilisateur créé'
                ]);
            }else{
                self::afficherErreur("Mots de passe distincts");
            }
        } else {
            // Appeler la méthode afficherErreur en cas de problème
            self::afficherErreur("Les paramètres de création de l'utilisateur sont incomplets.");
        }
    }

    public static function supprimer(): void
    {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];

            (new UtilisateurRepository())->supprimer($login);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVueGenerale('utilisateur/utilisateurSupprime.php', [
                'login' => $login,
                'utilisateurs' => $utilisateurs
            ]);
        } else {
            self::afficherErreur("Login manquant pour la suppression.");
        }
    }


    public static function afficherErreur(string $messageErreur = "") : void {
        $messageErreur = $messageErreur ? "Problème : " . htmlspecialchars($messageErreur) : "Problème avec l'utilisateur.";
        self::afficherVueGenerale('utilisateur/erreur.php', ['messageErreur' => $messageErreur, 'titre' => 'Erreur']);
    }


    public static function afficherFormulaireMiseAJour() : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($utilisateur) {
            $titre = "Mise à jour de l'utilisateur";
            self::afficherVueGenerale('utilisateur/formulaireMiseAJour.php', [
                'utilisateur' => $utilisateur,
                'titre' => $titre
            ]);
        } else {
            self::afficherErreur("Utilisateur non trouvé.");
        }
    }



    public static function mettreAJour() : void {
        if (isset($_GET['login'], $_GET['nom'], $_GET['prenom'])) {
            $login = $_GET['login'];
            $nom = $_GET['nom'];
            $prenom = $_GET['prenom'];

            // Récupérer l'utilisateur à partir du login
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

            if ($utilisateur) {
                // Mettre à jour les informations
                $utilisateur->setNom($nom);
                $utilisateur->setPrenom($prenom);

                // Appeler la méthode de mise à jour
                (new UtilisateurRepository())->mettreAJour($utilisateur); // Utilisation de l'instance

                self::afficherVueGenerale('utilisateur/utilisateurMisAJour.php', [
                    'login' => $login,
                    'utilisateurs' => (new UtilisateurRepository())->recuperer() // Méthode d'instance pour recuperer()
                ]);
            } else {
                self::afficherErreur("L'utilisateur à mettre à jour est introuvable.");
            }
        } else {
            self::afficherErreur("Données manquantes pour la mise à jour.");
        }
    }

  /*  public static function deposerCookie() : void
    {
        Cookie::enregistrer("TestCookie", 555, time() + 3600);
    }

    public static function lireCookie()
    {
        echo Cookie::lire("TestCookie");
    }

    public static function tkt()
    {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Cathy Penneflamme");
    }*/


}

