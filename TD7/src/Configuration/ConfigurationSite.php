<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private int $session_timeOut = 1800;

    public static function getSessionTimeout(): int
    {
        return self::$session_timeOut;
    }
}
