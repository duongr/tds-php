<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository
{
    /** @return string[] */
    protected function getNomsColonnes(): array {
        return ["login", "nom", "prenom", "mdpHache"];
    }

    protected function getNomTable(): string {
        return 'utilisateur';
    }
    protected function getNomClePrimaire(): string {
        return 'login';  // Clé primaire de la table utilisateur
    }

    // Construit un objet Utilisateur à partir d'un tableau SQL
    protected function construireDepuisTableauSQL(array $utilisateurTableau): AbstractDataObject {
        return new Utilisateur(
            $utilisateurTableau['login'],
            $utilisateurTableau['nom'],
            $utilisateurTableau['prenom'],
            MotDePasse::hacher($utilisateurTableau['mdpHache'])
        );
    }
    protected function formatTableauSQL(AbstractDataObject $utilisateur): array {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache()
        );
    }

    public function creerUtilisateurDepuisTableau(array $utilisateurTableau): Utilisateur {
        return $this->construireDepuisTableauSQL($utilisateurTableau);
    }





    /*
    public static function mettreAJour(Utilisateur $utilisateur): bool {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "UPDATE utilisateur SET nom = :nom, prenom = :prenom WHERE login = :login";
        $stmt = $pdo->prepare($sql);
        return $stmt->execute([
            'nom' => $utilisateur->getNom(),
            'prenom' => $utilisateur->getPrenom(),
            'login' => $utilisateur->getLogin()
        ]);
    }
    */

}