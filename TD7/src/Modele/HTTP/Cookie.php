<?php
namespace App\Covoiturage\Modele\HTTP;

class Cookie{
    // Enregistre un cookie avec une clé, une valeur et une durée d'expiration (en secondes)
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        if ($dureeExpiration == null) {
            $dureeExpiration = 0;  // Le cookie sera supprimé à la fin de la session si $dureeExpiration est null
        }
        setcookie($cle, serialize($valeur), time()+40000000);
    }

    // Lit le contenu d'un cookie
    public static function lire(string $cle): mixed {
        // Vérifier que le cookie existe avant de l'utiliser
        return isset($_COOKIE[$cle]) ? unserialize($_COOKIE[$cle]) : " ";
    }

    // Vérifie si un cookie existe
    public static function contient(string $cle): bool {
        return isset($_COOKIE[$cle]);
    }

    // Supprime un cookie
    public static function supprimer(string $cle): void {
        if (isset($_COOKIE[$cle])) {
            // Invalider le cookie en définissant une expiration passée
            setcookie($cle, "", time() - 3600, "/");
            unset($_COOKIE[$cle]);  // Supprime également la variable dans $_COOKIE côté serveur
        }
    }
}
