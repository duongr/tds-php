<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;

        $prenom = "Marc";
        $nom = "Evans";
        $login = "zedou";

        $utilisateur = [
                'nom' => 'Evans',
                'prenom' => 'Marc',
                'login' => 'zedou'
        ];

        $lst_utilisateur = [
                //$utilisateur,
        ];


        echo "<h1>Liste des utilisateurs :</h1>";

        if (sizeof($lst_utilisateur) == 0){
            echo "Il n’y a aucun utilisateur.";
        } else {
            echo "<ul>";
            foreach ($lst_utilisateur as $cle => $valeur){
                echo "<li>";
                echo "Nom : $valeur[nom] <br/>Prenom : $valeur[prenom] <br/>Login : $valeur[login]";
                echo "</li>";
            }
            echo "</ul>";
        }
        //var_dump($lst_utilisateur);


        //var_dump($utilisateur);

        //echo "ModeleUtilisateur $utilisateur[prenom] $utilisateur[nom] de login $utilisateur[login]";


        ?>
    </body>
</html> 