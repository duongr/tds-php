<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $ut = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('../vue/utilisateur/liste.php',["utilisateurs" => $ut]);
    }

    public static function afficherDetail() : void {
        $ut = ModeleUtilisateur::recupererUtilisateurParLogin($_GET["login"]);
        if ($ut == null){
            ControleurUtilisateur::afficherVue('../vue/utilisateur/erreur.php',[]);
        }
        ControleurUtilisateur::afficherVue('../vue/utilisateur/detail.php',["utilisateur" => $ut]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('../vue/utilisateur/formulaireCreation.php',[]);
    }

    public static function creerDepuisFormulaire() : void
    {
        $ut = new ModeleUtilisateur($_GET['login'],$_GET['nom'],$_GET['prenom']);
        $ut->ajouter();
        ControleurUtilisateur::afficherListe();
    }
}
?>
