<?php
namespace App\Covoiturage\Controleur;
use  App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur; // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $ut = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php',["utilisateurs" => $ut,"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail() : void {
        $ut = ModeleUtilisateur::recupererUtilisateurParLogin($_GET["login"]);
        if ($ut == null){
            ControleurUtilisateur::afficherVue('../vue/vueGenerale.php',["titre" => "Détail utilisateurs", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php',["utilisateur" => $ut,"titre" => "Détail utilisateurs", "cheminCorpsVue" => "utilisateur/detail.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php',["titre" => "Formulaire Création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void
    {
        $ut = new ModeleUtilisateur($_GET['login'],$_GET['nom'],$_GET['prenom']);
        $ut->ajouter();
        $uts = ModeleUtilisateur::recupererUtilisateurs();
        ControleurUtilisateur::afficherVue('../vue/vueGenerale.php',["utilisateurs" => $uts,"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }
}
?>
