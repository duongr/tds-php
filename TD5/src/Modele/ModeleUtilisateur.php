<?php
namespace App\Covoiturage\Modele;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
class ModeleUtilisateur
{
    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        for ($i = 0; $i < 64; $i++) {
            $this->login = $this->login . $login[$i];
        }
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }


    // Pour pouvoir convertir un objet en chaîne de caractères
    /*
    public function __toString(): string
    {
        // À compléter dans le prochain exercice
        return "ModeleUtilisateur $this->prenom $this->nom de login $this->login";
    }
    */

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        $utilisateurs = new ModeleUtilisateur($utilisateurFormatTableau['login'],$utilisateurFormatTableau['nom'],$utilisateurFormatTableau['prenom']);
        return $utilisateurs;
    }

    public static function recupererUtilisateurs() : array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau){
            return null;
        }

        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void{
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
        $pdoStatement = $pdo->prepare($sql);
        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom,
        );
        $pdoStatement->execute($values);
    }



}