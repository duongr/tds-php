<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Créer un utilisateur</title>
    <link rel="stylesheet" href="../../../ressources/css/navstyle.css">
</head>
<body>
<h1>Création d'un nouvel utilisateur</h1>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="creerDepuisFormulaire">
    <input type="hidden" name="controleur" value="utilisateur"> <!-- Ce champ est nécessaire -->

    <!-- Champs du formulaire -->
    <fieldset>
        <legend>Mon formulaire :</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Niko" name="prenom" id="prenom_id" />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Bellic" name="nom" id="nom_id" />
        </p>

        <p>
            <input class="btn-submit" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>


</body>
</html>
