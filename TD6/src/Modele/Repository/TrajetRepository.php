<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use PDO;
use DateTime;

class TrajetRepository extends AbstractRepository {

    protected function getNomTable(): string {
        return 'trajet';
    }
    protected function getNomClePrimaire(): string {
        return 'id';  // Clé primaire de la table trajet
    }
    protected function construireDepuisTableauSQL(array $trajetTableau): Trajet {
        // Récupérer le conducteur via UtilisateurRepository (appel non statique)
        $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau['conducteurLogin']);

        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            $conducteur,
            $trajetTableau["nonFumeur"]
        );
        // Récupérer les passagers en utilisant un appel dynamique
        $trajet->setPassagers($this->recupererPassagers($trajet));

        return $trajet;
    }

    public static function recupererTrajets(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM trajet";
        $stmt = $pdo->query($sql);

        $trajets = [];
        while ($trajetTableau = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $trajets[] = self::construireDepuisTableauSQL($trajetTableau);
        }

        return $trajets;
    }

    public function recupererTrajetParId(int $id): ?Trajet {
        return $this->recupererParClePrimaire($id); // Appel dynamique de recupererParClePrimaire
    }

    /*
    public static function ajouterTrajet(Trajet $trajet): void {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur)
                VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            'depart' => $trajet->getDepart(),
            'arrivee' => $trajet->getArrivee(),
            'date' => $trajet->getDate()->format('Y-m-d'),
            'prix' => $trajet->getPrix(),
            'conducteurLogin' => $trajet->getConducteur()->getLogin(),
            'nonFumeur' => $trajet->isNonFumeur() ? 1 : 0
        ]);
    }
    */

    // Cette méthode est appelée dynamiquement depuis construireDepuisTableauSQL()
    public static function recupererPassagers(Trajet $trajet): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT u.* FROM passager p JOIN utilisateur u ON p.passagerLogin = u.login WHERE p.trajetId = :trajetId";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['trajetId' => $trajet->getId()]);

        $passagers = [];
        while ($utilisateurTableau = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // Utilisation de l'appel dynamique pour UtilisateurRepository
            $passagers[] = (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurTableau);
        }

        return $passagers;
    }
    protected function getNomsColonnes(): array {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }
    protected function formatTableauSQL(AbstractDataObject $object): array
    {
        /** @var Trajet $trajet */
        $trajet = $object;
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format('Y-m-d'),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur() ? 1 : 0,
        );
    }

    public function mettreAJour(AbstractDataObject $objet): void {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "UPDATE trajet SET depart = :departTag, arrivee = :arriveeTag, date = :dateTag, prix = :prixTag, 
            conducteurLogin = :conducteurLoginTag, nonFumeur = :nonFumeurTag WHERE id = :idTag";
        $stmt = $pdo->prepare($sql);
        $stmt->execute($this->formatTableauSQL($objet));
    }


}
?>